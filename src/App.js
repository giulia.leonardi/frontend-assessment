import "./App.css";
import ProductBuilder from "./components/ProductBuilder/ProductBuilder";

function App() {
  return <ProductBuilder />;
}

export default App;
