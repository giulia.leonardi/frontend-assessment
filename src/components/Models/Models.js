import React, { useContext } from "react";
import "./Models.css";
import ChoicesConxect from "../Choices";
import Model from "./ModelPreview";

const Models = (props) => {
  const choices = useContext(ChoicesConxect);
  return (
    <>
      <a
        href="https://codyhouse.co/gem/product-builder"
        className="nugget-info hide-on-desktop"
      >
        Article &amp; Download
      </a>
      <ul className="models-list options-list col-2">
        {choices.map((item, key) => {
          return (
            <Model
              key={key}
              item={item}
              myModel={props.myModel}
              setMyModel={props.setMyModel}
            />
          );
        })}
      </ul>
    </>
  );
};

export default Models;
