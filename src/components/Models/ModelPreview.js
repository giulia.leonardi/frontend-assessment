import React, { useState } from "react";
import "./Models.css";

const Model = (props) => {
  const [selected, setSelected] = useState(false);

  React.useEffect(() => {
    setSelected(props.myModel?.info.model === props.item.info.model);
  }, [props.myModel, props.item.info.model, setSelected]);

  return (
    <li
      className={selected ? " selected loaded" : ""}
      data-price={props.item.info.price}
      data-model={props.item.info.model}
      onClick={() =>
        props.setMyModel(props.myModel === props.item ? null : props.item)
      }
    >
      <span className="name">{props.item.info.name}</span>
      <img
        src={
          "/img/" +
          props.item.info.model +
          "_" +
          props.item.colors[0].id +
          ".jpeg"
        }
        alt={props.item.info.name}
      />
      <span className="price">
        {"from $" +
          props.item.info.price
            .toString()
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}
      </span>
      <div className="radio" />
    </li>
  );
};
export default Model;
