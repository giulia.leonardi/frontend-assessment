import React, { useContext } from "react";
import "./Footer.css";
import StepContext from "../Step";

function Footer(props) {
  const steps = useContext(StepContext);
  return (
    <footer
      className={
        "builder-footer" +
        (props.disabled ? " disabled" : "") +
        (props.index === 0 ? " step-1" : "") +
        (props.showAlert ? " show-alert" : "")
      }
    >
      <div className="selected-product">
        <img src={props.img} alt="Product preview" />
        <div className="tot-price">
          <span>Total</span>
          <span className="total">
            $<b>{props.total}</b>
          </span>
        </div>
      </div>
      <nav className="builder-secondary-nav">
        <ul>
          <li className="next nav-item">
            <ul>
              {steps.map((step, key) => {
                return (
                  <li
                    key={key}
                    className={step.id === props.index ? "visible" : ""}
                    onClick={() => {
                      if (props.disabled) {
                        props.setShowAlert(true);
                      } else {
                        if (props.index < steps.length - 1) {
                          props.setIndex(props.index + 1);
                        }
                      }
                    }}
                  >
                    <a href="#0">{step.next}</a>
                  </li>
                );
              })}
            </ul>
          </li>
          <li className="prev nav-item">
            <ul>
              {steps.map((step, key) => {
                return (
                  <li
                    key={key}
                    className={step.id === props.index ? "visible" : ""}
                    onClick={() => {
                      if (props.index > 0) {
                        props.setIndex(props.index - 1);
                      }
                    }}
                  >
                    <a href="#0">{step.prev}</a>
                  </li>
                );
              })}
            </ul>
          </li>
        </ul>
      </nav>
      <span className="alert">Please, select a model first!</span>
    </footer>
  );
}

export default Footer;
