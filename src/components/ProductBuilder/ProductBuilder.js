import React, { useState } from "react";
import "./ProductBuilder.css";
import Header from "../Header/Header";
import BuilderSteps from "../BuilderSteps/BuilderSteps";
import Models from "../Models/Models";
import Colors from "../Colors/Colors";
import Accessories from "../Accessories/Accessories";
import Summary from "../Summary/Summary";
import StepContext from "./../Step";
import Footer from "../Footer/Footer";

const ProductBuilder = () => {
  const [index, setIndex] = useState(0);
  const steps = [
    {
      id: 0,
      name: "Models",
      title: "Select Model",
      children: <Models />,
      next: "Colors",
      prev: "Models",
    },
    {
      id: 1,
      name: "Colors",
      title: "Select Color",
      children: <Colors />,
      next: "Accessories",
      prev: "Models",
    },
    {
      id: 2,
      name: "Accessories",
      title: "Accessories",
      children: <Accessories />,
      next: "Summary",
      prev: "Colors",
    },
    {
      id: 3,
      name: "Summary",
      title: "Summary",
      children: <Summary />,
      next: "Buy Now",
      prev: "Accessories",
    },
  ];
  const [myModel, setMyModel] = useState(null);
  const [myColor, setMyColor] = useState(null);
  const [myAccessories, setMyAccessories] = useState([]);
  const [goNext, setGoNext] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  React.useEffect(() => {
    setMyColor(myModel?.colors[0]);
    setGoNext(myModel === null ? false : true);
  }, [myModel]);

  return (
    <StepContext.Provider value={steps}>
      <div className="cd-product-builder">
        <Header
          index={index}
          onChangeStep={setIndex}
          goNext={goNext}
          setShowAlert={setShowAlert}
        />
        <BuilderSteps
          index={index}
          myModel={myModel}
          setMyModel={setMyModel}
          myColor={myColor}
          setMyColor={setMyColor}
          myAccessories={myAccessories}
          setMyAccessories={setMyAccessories}
        />
        <Footer
          disabled={myModel === null}
          index={index}
          setIndex={setIndex}
          showAlert={showAlert}
          setShowAlert={setShowAlert}
          img={myColor?.img || myModel?.img}
          total={
            (myModel?.info.price || 0) +
            (myColor?.price || 0) +
            myAccessories.reduce((a, b) => a + b.price, 0)
          }
        />
      </div>
    </StepContext.Provider>
  );
};

export default ProductBuilder;
