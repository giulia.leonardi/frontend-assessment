import React from "react";
import "./Accessories.css";
import AccessoryPreview from "./AccessoryPreview";

const Accessories = (props) => {
  return (
    <ul className="accessories-list options-list">
      {props.myModel?.accessories.map((accessory, key) => {
        return (
          <AccessoryPreview
            key={key}
            accessory={accessory}
            myAccessories={props.myAccessories}
            setMyAccessories={props.setMyAccessories}
          />
        );
      })}
    </ul>
  );
};

export default Accessories;
