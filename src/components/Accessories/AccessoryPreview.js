import React, { useState } from "react";
import "./Accessories.css";

function containsObject(obj, list) {
  var i;
  for (i = 0; i < list?.length; i++) {
    if (list[i] === obj) {
      return true;
    }
  }

  return false;
}

const AccessoryPreview = (props) => {
  const [selected, setSelected] = useState(false);

  React.useEffect(() => {
    setSelected(containsObject(props.accessory, props.myAccessories));
  }, [props.myAccessories, props.accessory, setSelected]);

  return (
    <li
      className={selected ? " selected" : ""}
      data-price={props.accessory.price}
      onClick={() => {
        if (containsObject(props.accessory, props.myAccessories)) {
          let temp = props.myAccessories;
          let index = temp.indexOf(props.accessory);
          if (index > -1) {
            temp.splice(index, 1);
            props.setMyAccessories([...temp]);
          }
        } else {
          props.setMyAccessories([...props.myAccessories, props.accessory]);
        }
      }}
    >
      <p>{props.accessory.name}</p>
      <span className="price">
        {"$" +
          props.accessory.price
            .toString()
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}
      </span>
      <div className="check" />
    </li>
  );
};

export default AccessoryPreview;
