import React from "react";
import "./Summary.css";
const Summary = (props) => {
  return (
    <ul className="summary-list">
      <li>
        <h2>Model</h2>
        <img
          src={props.myColor?.img}
          alt="Alfa Romeo Giulietta"
          className="product-preview"
        />
        <h3>{props.myModel?.info.name}</h3>
        <p>{props.myModel?.info.description}</p>
      </li>
      <li data-summary="colors">
        <h2>Color</h2>
        <span className="summary-color">
          <em className="color-swatch" data-color={props.myColor?.data} />
          <em className="color-label">
            {props.myColor?.name + " - $" + props.myColor?.price}
          </em>
        </span>
      </li>
      <li data-summary="accessories">
        <h2>Accessories</h2>
        <ul className="summary-accessories">
          {props.myAccessories?.length > 0 ? (
            props.myAccessories?.map((accessory, key) => {
              return (
                <li key={key}>
                  <p>{accessory.name}</p>
                </li>
              );
            })
          ) : (
            <li>
              <p>No Accessories selected;</p>
            </li>
          )}
        </ul>
      </li>
    </ul>
  );
};

export default Summary;
