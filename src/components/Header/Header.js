import React, { useContext } from "react";
import "./Header.css";
import StepContext from "../Step";

const Header = (props) => {
  const steps = useContext(StepContext);
  return (
    <header className="main-header">
      <h1>Product Builder</h1>
      <nav className="builder-main-nav disabled">
        <ul>
          {steps.map((step, key) => {
            return (
              <li key={key} className={step.id === props.index ? "active" : ""}>
                <a
                  href={"#" + step.name.toLowerCase()}
                  onClick={() => {
                    if (props.goNext) {
                      props.onChangeStep(key);
                      props.setShowAlert(false);
                    } else {
                      if (step.id > 0) {
                        props.setShowAlert(true);
                      }
                    }
                  }}
                >
                  {step.name}
                </a>
              </li>
            );
          })}
        </ul>
      </nav>
      <a
        href="https://codyhouse.co/gem/product-builder"
        className="nugget-info hide-on-mobile"
      >
        Article &amp; Download
      </a>
    </header>
  );
};

export default Header;
