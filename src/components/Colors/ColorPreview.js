import React, { useState } from "react";

const ColorPreview = (props) => {
  const [selected, setSelected] = useState(null);

  React.useEffect(() => {
    setSelected(props.myColor?.id === props.color.id);
  }, [props.myColor, props.color.id, setSelected]);

  return (
    <li className={selected ? " selected loaded" : ""}>
      <img
        src={props.color.img}
        alt="Product Preview"
        className="product-preview"
      />
    </li>
  );
};

export default ColorPreview;
