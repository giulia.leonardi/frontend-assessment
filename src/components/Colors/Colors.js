import React from "react";
import ColorCustomizer from "./ColorCustomizer";
import ColorPreview from "./ColorPreview";
import "./Colors.css";

const Colors = (props) => {
  return (
    <>
      <ul className="product-previews">
        {props.myModel?.colors.map((color, key) => {
          return (
            <ColorPreview
              key={key}
              color={color}
              myColor={props.myColor}
              setMyColor={props.setMyColor}
            />
          );
        })}
      </ul>
      <ul className="product-customizer">
        {props.myModel?.colors.map((color, key) => {
          return (
            <ColorCustomizer
              key={key}
              color={color}
              myColor={props.myColor}
              setMyColor={props.setMyColor}
            />
          );
        })}
      </ul>
    </>
  );
};

export default Colors;
