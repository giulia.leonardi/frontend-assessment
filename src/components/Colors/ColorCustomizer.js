import React, { useState } from "react";

const ColorCustomizer = (props) => {
  const [selected, setSelected] = useState(null);

  React.useEffect(() => {
    setSelected(props.myColor?.id === props.color.id);
  }, [props.myColor, props.color.id, setSelected]);

  return (
    <li
      data-content={props.color.name + " - $" + props.color.price}
      data-price={props.color.price}
      className={selected ? " selected" : ""}
    >
      <a
        data-color={props.color.data}
        href="#0"
        onClick={() => props.setMyColor(props.color)}
      >
        {props.color.name + " - $" + props.color.price}
      </a>
    </li>
  );
};

export default ColorCustomizer;
