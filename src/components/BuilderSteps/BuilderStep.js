import React from "react";
import "./BuilderSteps.css";

function BuilderStep(props) {
  return (
    <li
      data-selection={props.item.name.toLowerCase()}
      className={
        "builder-step" + (props.index === props.item.id ? " active" : "")
      }
    >
      <section className="step-content">
        <header>
          <h1>{props.item.title}</h1>
          <span className="steps-indicator">
            Step <b>{props.step + 1}</b> of {props.totalSteps}
          </span>
        </header>
        {props.children}
      </section>
    </li>
  );
}

export default BuilderStep;
