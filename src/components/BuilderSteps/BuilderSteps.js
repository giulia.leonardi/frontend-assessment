import React, { useContext } from "react";
import "./BuilderSteps.css";
import BuilderStep from "./BuilderStep";
import ChoicesConxect from "../Choices";
import StepContext from "../Step";

const models = [
  {
    info: {
      model: "product01",
      name: "BMW i3",
      img: "/img/product01_col01.jpeg",
      price: 42400,
      description:
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit saepe facilis hic, unde, numquam vel. Blanditiis sed laboriosam ratione nulla atque molestias at explicabo aperiam reprehenderit culpa nihil, quis totam cupiditate dolores in quisquam magnam inventore nobis, rem adipisci eveniet illum.",
    },
    colors: [
      {
        id: "col01",
        name: "White",
        price: 0,
        img: "/img/product01_col01.jpeg",
        data: "white",
      },
      {
        id: "col02",
        name: "Mineral Grey",
        price: 550,
        img: "/img/product01_col02.jpeg",
        data: "grey",
      },
      {
        id: "col03",
        name: "Orange Metallic",
        price: 550,
        img: "/img/product01_col03.jpeg",
        data: "orange",
      },
    ],
    accessories: [
      { name: "BMW Charging Station", price: 1080 },
      { name: "BMW Maintenance Program Upgrade", price: 1895 },
      { name: "1 Year BMW Maintenance Program Upgrade", price: 975 },
    ],
  },
  {
    info: {
      model: "product02",
      name: "BMW i8",
      img: "/img/product01_col01.jpeg",
      price: 140700,
      description:
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit saepe facilis hic, unde, numquam vel. Blanditiis sed laboriosam ratione nulla atque molestias at explicabo aperiam reprehenderit culpa nihil, quis totam cupiditate dolores in quisquam magnam inventore nobis, rem adipisci eveniet illum.",
    },
    colors: [
      {
        id: "col01",
        name: "Grey Metallic",
        price: 0,
        img: "/img/product02_col01.jpeg",
        data: "grey",
      },
      {
        id: "col02",
        name: "White Perl Metallic",
        price: 1800,
        img: "/img/product02_col02.jpeg",
        data: "perl",
      },
    ],
    accessories: [
      { name: "BMW Laserlight", price: 6300 },
      { name: "BMW Charging Station", price: 1080 },
      { name: "BMW Maintenance Program Upgrade", price: 1895 },
      { name: "1 Year BMW Maintenance Program Upgrade", price: 975 },
    ],
  },
];

const BuilderSteps = (props) => {
  const steps = useContext(StepContext);

  return (
    <ChoicesConxect.Provider value={models}>
      <div className="builder-steps">
        <ul>
          {steps.map((item, key) => {
            return (
              <BuilderStep
                key={key}
                item={item}
                step={key}
                totalSteps={steps.length}
                index={props.index}
              >
                {React.cloneElement(item.children, {
                  myModel: props.myModel,
                  setMyModel: props.setMyModel,
                  myColor: props.myColor,
                  setMyColor: props.setMyColor,
                  myAccessories: props.myAccessories,
                  setMyAccessories: props.setMyAccessories,
                })}
              </BuilderStep>
            );
          })}
        </ul>
      </div>
    </ChoicesConxect.Provider>
  );
};

export default BuilderSteps;
